import logging

from docutils.io import StringInput
from docutils.nodes import document
from docutils.parsers import get_parser_class
from docutils.readers.standalone import Reader as StandaloneReader

from .rst import register

class Settings:
    tab_width = 4
    report_level = logging.INFO
    halt_level = logging.CRITICAL
    warning_stream = None
    debug = False
    error_encoding = None
    error_encoding_error_handler = None
    language_code = 'en'
    id_prefix = 'id-'

    # pep_references = None

    def __init__(self):
        self._unknown_accessed = []
        # currently accessed:
        # character_level_inline_markup
        # pep_references
        # rfc_references
        # __wrapped__

    def __getattr__(self, item):
        self._unknown_accessed.append(item)
        return None


def parse(source: str) -> document:
    parser = get_parser_class('restructuredText')()
    reader = StandaloneReader(parser=parser)
    return reader.read(StringInput(source), parser=None, settings=Settings())


def test_lemma():
    register()
    parsed = parse("""
.. lemma:: Lemma of Zorn
   :label: zorn
  
   If `x` is a poset in which every chain has an upper bound,
   then `x` has a maximal element.
""")
    print(parsed.pformat())
    assert parsed.pformat() == """\
<document source="<string>">
    <Lemma label="lem:zorn" title="Lemma of Zorn">
        <paragraph>
            If 
            <InlineMath>
                x
             is a poset in which every chain has an upper bound,
            then 
            <InlineMath>
                x
             has a maximal element.
"""


def test_proof():
    register()
    parsed = parse(r"""
.. proof::
   :proves: nil_codim1
   
   Let :math:`I≤L` be a maximal subalgebra.
   Our strategy is to find an :math:`x\in L\setminus I` normalizing the latter.
""")

    print(parsed.pformat())
    assert parsed.pformat() == r"""
<document source="<string>">
    <Proof>
        <paragraph>
            Let 
            <InlineMath>
                I≤L
             be a maximal subalgebra.
            Our strategy is to find an 
            <InlineMath>
                x\in L\setminus I
             normalizing the latter.
""".lstrip()
