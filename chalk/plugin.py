from io import StringIO

from lektor.pluginsystem import Plugin
from lektor.types import Type
from markupsafe import Markup

from .rst import setup_publisher, register


class ChalkPlugin(Plugin):
    name = 'chalk'
    description = ''

    def on_setup_env(self, **extra):
        self.env.add_type(MathRstType)
        self.env.jinja_env.globals['chalk_mathjax_tags'] = MATHJAX_TAGS

        register()


MATHJAX_TAGS = r"""
<script type="text/x-mathjax-config">
        MathJax.Hub.Config({
          tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}
          });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML' async></script>
"""


class MathRstType(Type):
    name = 'mathrst'
    widget = 'multiline-text'

    def value_from_raw(self, raw):
        register()
        pub = setup_publisher()

        pub.set_source(source=StringIO(raw.value))
        pub.publish()

        return Markup(pub.writer.parts['fragment'])
