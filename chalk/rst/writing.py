from docutils.writers import html4css1

from .nodes import InlineMath, Statement, Proof


# noinspection PyPep8Naming
class ExtendedHtmlTranslator(html4css1.HTMLTranslator):
    def __init__(self, *a, **kw):
        self.statement_counter = 0
        super().__init__(*a, **kw)

    def dispatch_visit(self, node):
        if isinstance(node, Statement):
            return self.visit_Statement(node)
        return super().dispatch_visit(node)

    def dispatch_departure(self, node):
        if isinstance(node, Statement):
            return self.depart_Statement(node)
        return super().dispatch_departure(node)

    def visit_InlineMath(self, _: InlineMath):
        self.body.append(r'\(')

    def depart_InlineMath(self, _: InlineMath):
        self.body.append(r'\)')

    visit_role = visit_InlineMath
    depart_role = depart_InlineMath

    def visit_literal(self, node):
        raise Exception

    def visit_Statement(self, node: Statement):
        type_ = node.blockname  # TODO give more information, such as title
        if node['should_count']:
            self.statement_counter += 1
            title = f"{type_.capitalize()} {self.statement_counter}"
        else:
            title = f"{type_.capitalize()}"
        self.body.append(f'<div class="math-block math-block-{type_}">')
        self.body.append(f'<div class="math-block-title">{title}</div>')
        self.body.append('<div class="math-block-content">')

    def depart_Statement(self, node: Statement):
        self.body.append('</div>')  # math-block-content
        self.body.append('</div>')  # math-block

    def visit_Proof(self, node: Proof):
        # TODO support alternative descriptions
        desc = 'Proof'
        self.body.append(f'<div class="math-proof">')
        self.body.append(f'<span class="math-proof-keyword">{desc}</span>')
        self.body.append('<div class="math-proof-content">')

    def depart_Proof(self, node: Proof):
        self.body.append('</div>')  # math-proof-content
        self.body.append('</div>')  # math-block


class ExtendedHtmlWriter(html4css1.Writer):
    def __init__(self, *a, **kw):
        super().__init__(*a, **kw)
        self.translator_class = ExtendedHtmlTranslator
