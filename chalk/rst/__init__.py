import docutils
from docutils.core import Publisher
from docutils.parsers.rst import directives, roles
from docutils.parsers.rst.roles import GenericRole

from .nodes import InlineMath
from .parsing import register_directives
from .writing import ExtendedHtmlWriter


def setup_publisher() -> Publisher:
    pub = Publisher(
        destination_class=docutils.io.StringOutput,
        reader=docutils.readers.get_reader_class('standalone')(),
        writer=ExtendedHtmlWriter(),
        parser=docutils.parsers.get_parser_class('restructuredText')(),
    )
    # an alternative would be:
    # pub.set_components(reader_name='standalone',
    #                    parser_name='restructuredText',
    #                    writer_name='html')

    pub.process_programmatic_settings(
        None,
        {'doctitle_xform': False,
         'initial_header_level': '2',
         'syntax_highlight': 'short'},
        None)

    return pub


def register():
    register_directives()

    math_role = GenericRole('math', InlineMath)
    roles.register_canonical_role('math', math_role)
    # noinspection PyProtectedMember
    roles._roles[''] = math_role
