from abc import ABC, abstractmethod
from typing import Type, Optional

from docutils.nodes import Node
from docutils.parsers.rst import Directive
from docutils.parsers.rst.directives import unchanged, register_directive, flag

from .nodes import Proof, statement_node


class StatementDirective(Directive, ABC):
    required_arguments = 0
    optional_arguments = 1
    option_spec = {'label': unchanged, 'nocount': flag}

    has_content = True
    final_argument_whitespace = True

    @property
    @abstractmethod
    def node_class(self) -> Type[Node]:
        raise NotImplementedError

    @property
    @abstractmethod
    def label_prefix(self) -> str:
        raise NotImplementedError

    def run(self):
        self.assert_has_content()
        title_text = self.arguments[0] if self.arguments else ''
        # title_text_nodes, messages = self.state.inline_text(title_text, self.lineno)
        # title = nodes.title(title_text, '', *title_text_nodes)
        text = '\n'.join(self.content)

        node = self.node_class(text)
        node['title'] = title_text
        node['label'] = self.label
        node['should_count'] = self.should_count

        self.add_name(node)

        if text:
            self.state.nested_parse(self.content, self.content_offset, node)
        return [node]

    @property
    def label(self) -> Optional[str]:
        if 'label' in self.options:
            prefix = self.options.get('label-prefix', self.label_prefix)
            return f"{prefix}{self.options['label']}"
        return None

    @property
    def should_count(self) -> bool:
        return 'nocount' not in self.options


class LemmaDirective(StatementDirective):
    node_class = statement_node('Lemma')
    label_prefix = 'lem:'


class TheoremDirective(StatementDirective):
    node_class = statement_node('theorem')
    label_prefix = 'thm:'


class CorollaryDirective(StatementDirective):
    node_class = statement_node('corollary')
    label_prefix = 'cor:'


class ConjectureDirective(StatementDirective):
    node_class = statement_node('conjecture')
    label_prefix = 'conj:'


class QuestionDirective(StatementDirective):
    node_class = statement_node('question')
    label_prefix = 'q:'


class ObservationDirective(StatementDirective):
    node_class = statement_node('observation')
    label_prefix = 'obs:'


class IdeaDirective(StatementDirective):
    node_class = statement_node('idea')
    label_prefix = 'idea:'


class DefinitionDirective(StatementDirective):
    node_class = statement_node('definition')
    label_prefix = 'def:'


class ExampleDirective(StatementDirective):
    node_class = statement_node('example')
    label_prefix = 'ex:'


class ProofDirective(Directive):
    has_content = True
    final_argument_whitespace = True
    option_spec = {'proves': unchanged}

    node_class = Proof

    def run(self):
        self.assert_has_content()

        node = self.node_class(rawsource='\n'.join(self.content))

        self.state.nested_parse(self.content, self.content_offset, node)
        return [node]


def register_directives():
    register_directive('lemma', LemmaDirective)
    register_directive('proof', ProofDirective)
    register_directive('theorem', TheoremDirective)
    register_directive('corollary', CorollaryDirective)
    register_directive('conjecture', ConjectureDirective)
    register_directive('question', QuestionDirective)
    register_directive('observation', ObservationDirective)
    register_directive('idea', IdeaDirective)
    register_directive('definition', DefinitionDirective)
    register_directive('example', ExampleDirective)

