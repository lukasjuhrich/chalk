from abc import ABC, abstractmethod

from docutils.nodes import General, Element, Part, raw, unescape, inline, section


class InlineMath(inline):
    def __init__(self, rawsource, text, *children, **attributes):
        super().__init__(rawsource, unescape(text, restore_backslashes=True),
                         *children, **attributes)


class Statement(section, ABC):
    @property
    @abstractmethod
    def blockname(self) -> str:
        raise NotImplementedError


def statement_node(blockname: str):
    return type(f"{blockname}Statement", (Statement,), {'blockname': blockname})


class Proof(Part, Element):
    pass
