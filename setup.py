from setuptools import setup

setup(
    name='lektor-chalk',
    version='0.1',
    packages=['chalk'],
    entry_points={
        'lektor.plugins': [
            'chalk = chalk:ChalkPlugin'
        ],
    },
    install_requires=[
        'docutils',
        'Lektor',
        'markupsafe',
    ],
)
